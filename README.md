cpuavg
===
## installing
```bash
cargo install --git https://gitlab.com/Lynnesbian/cpuavg.git client server
```

## usage
set `cpuavg-server` to run at startup with cron/systemd/a login script/whatever, then add something like
```bash
PS1+="$(cpuavg-client)"
```

to your `.bashrc` so you can see your CPU usage in your bash prompt, wow how cool is that

### command line arguments
both the client and the server support changing the port (default `59663`, which is
[T9](https://en.wikipedia.org/wiki/T9_(predictive_text)) for "LYNNE" hee hee hee) with the `--port` parameter.
`cpuavg-server` also supports setting the sample count (`--samples`) and rate (`--sample-rate`). also you can use
`-h` or `--help` to get help if you want.

## uh oh!!!
cargo currently has a [bug/misfeature/surprise mechanic](https://github.com/rust-lang/cargo/issues/4463) when you use
features with workspaces in a certain way. the TL;DR is that you should build the client (or server) with
`cargo build -p client`, and *not* `cargo build --bin cpuavg-client` or `cargo build --all`. if you do it those
other two ways, things will get all [sorceled up](https://clips-media-assets2.twitch.tv/AT-cm|394769079-preview.jpg)
and `cpuavg-client` will output the help message for `cpuavg-server` and oh jeez oh man oh no

## license
available under the venerable [MIT license](https://opensource.org/license/mit).
