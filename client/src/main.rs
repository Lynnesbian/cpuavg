use std::io::Read;
use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpStream};
use std::time::Duration;

use anyhow::Result;

fn main() {
	if read().is_err() {
		println!("???%");
	}
}

fn read() -> Result<()> {
	let args = common::Args::new()?;
	let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), args.port);

	let stream = TcpStream::connect_timeout(&addr, Duration::from_millis(100));
	if let Ok(mut stream) = stream {
		let mut load = String::with_capacity(6);

		stream.set_read_timeout(Some(Duration::from_millis(100)))?;
		stream.read_to_string(&mut load)?;

		println!("{}", load);
	} else {
		println!("...%");
		return Ok(());
	}

	Ok(())
}
