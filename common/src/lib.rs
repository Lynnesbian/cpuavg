use anyhow::Result;
use cfg_if::cfg_if;
use const_format::formatcp;

cfg_if! {
	if #[cfg(feature = "server")] {
		const BINARY: &str = "cpuavg-server";
	} else {
		const BINARY: &str = "cpuavg-client";
	}
}

// the line below gives a false warning on clippy stable w/ rust 1.57
#[allow(clippy::semicolon_if_nothing_returned)]
const HELP: &str = formatcp!(
	"\
{BINARY}

USAGE:
  {BINARY} [OPTIONS]

FLAGS:
  -h, --help            Prints help information

OPTIONS:
  --port NUMBER         Port to listen on [default: 59663]
"
);

#[cfg(feature = "server")]
const SERVER_HELP: &str = "\
  --samples NUMBER      Number of samples to keep for averaging [default: 6]
  --sample_rate NUMBER  Frequency to take samples, in milliseconds [default: 500]
";

pub struct Args {
	pub port: u16,
	#[cfg(feature = "server")]
	pub samples: usize,
	#[cfg(feature = "server")]
	pub sample_rate: u64,
}

impl Args {
	/// Creates an [`Args`] struct from the provided command-line arguments.
	/// # Errors
	/// An error will be returned if any of the arguments fail are invalid UTF-8.
	pub fn new() -> Result<Self> {
		let mut pargs = pico_args::Arguments::from_env();

		if pargs.contains(["-h", "--help"]) {
			Self::help_and_exit(exitcode::OK);
		}

		let args = Self {
			port: pargs.opt_value_from_str("--port")?.unwrap_or(59663),
			#[cfg(feature = "server")]
			samples: pargs.opt_value_from_str("--samples")?.unwrap_or(6),
			#[cfg(feature = "server")]
			sample_rate: pargs.opt_value_from_str("--sample-rate")?.unwrap_or(500),
		};

		if !pargs.finish().is_empty() {
			// unrecognised argument(s) were provided
			Self::help_and_exit(exitcode::USAGE);
		};

		Ok(args)
	}

	/// Prints [`HELP`] and exits with the provided exit code.
	fn help_and_exit(code: i32) {
		print!("{}", HELP);

		#[cfg(feature = "server")]
		// the spaces are needed because rust strips the leading indentation in SERVER_HELP
		print!("  {}", SERVER_HELP);

		println!();

		std::process::exit(code);
	}
}
