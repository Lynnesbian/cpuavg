use std::io::Write;
use std::net::TcpListener;

use anyhow::Result;

use crate::LOAD;

pub fn listen(listener: &TcpListener) -> Result<()> {
	for stream in listener.incoming() {
		match LOAD.lock() {
			Ok(load) => stream?.write_fmt(format_args!("{:.2}%", *load))?,
			Err(_) => {
				stream?.write_all(b"!!!%")?;
			}
		}
	}

	Ok(())
}
