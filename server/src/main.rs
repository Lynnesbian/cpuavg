mod listener;
mod measured_usage;

use std::net::TcpListener;
use std::sync::LazyLock;
use std::sync::Mutex;
use std::thread::sleep;
use std::time::Duration;

use anyhow::{anyhow, Context, Result};

pub static LOAD: LazyLock<Mutex<f64>> = LazyLock::new(|| Mutex::new(0.0));

use crate::measured_usage::MeasuredUsage;

fn main() -> Result<()> {
	let args = common::Args::new()?;
	let tcp_listener =
		TcpListener::bind(("127.0.0.1", args.port)).context(format!("Couldn't bind to port {}", args.port))?;
	std::thread::spawn(move || listener::listen(&tcp_listener));

	let mut history: Vec<MeasuredUsage> = Vec::with_capacity(args.samples);

	loop {
		history.push(MeasuredUsage::new().context("Couldn't measure CPU usage")?);

		if history.len() > args.samples {
			// remove the oldest measurement
			history.remove(0);
		}

		history.shrink_to_fit();

		// history contains the last SAMPLES measurements from /proc/stat.
		// /proc/stat records CPU usage from boot time. calculating usage based on the average of all the MeasuredUsage's
		// will give us the average CPU usage *since boot*, not the average CPU usage *over the past SAMPLES samples*. in
		// order to get that, we need to get the average of the *deltas* between pairs of MeasuredUsage's.

		// for example, if the first MeasuredUsage recorded an idle of "12345", and then the next MeasuredUsage recorded an
		// idle of "12355", the idle time between those two measurements is (12345 - 12355 =) 10.

		// to get this measurement, we split the history vec into pairs with the windows() method, and then sum the deltas
		// between each set of pairs. then, we compute the average by dividing this by the number of measurements made so
		// far (history.len()).

		if let Ok(mut load) = LOAD.lock() {
			*load = history
				.windows(2)
				.fold(0.0, |acc, usages| acc + MeasuredUsage::delta(&usages[0], &usages[1]).load())
				/ (history.len() - 1) as f64;
		} else {
			return Err(anyhow!("Couldn't lock LOAD!"));
		}

		sleep(Duration::from_millis(args.sample_rate));
	}
}
