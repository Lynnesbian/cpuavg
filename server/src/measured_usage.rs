use std::fmt::{Debug, Formatter};
use std::str::FromStr;

use anyhow::{Context, Result};

pub const IDLE_COLUMN: usize = 4;

#[derive(Copy, Clone, Default)]
pub struct MeasuredUsage {
	total: f64,
	idle: f64,
}

impl MeasuredUsage {
	/// Makes a single load measurement from /proc/stat.
	pub fn new() -> Result<Self> {
		let stat = std::fs::read_to_string("/proc/stat")?;
		let mut lines = stat.lines();

		// ignore first line - we want to calculate usage by summing individual CPUs rather than using the global stats
		lines.next().context("Unexpected EOF while reading /proc/stat")?;

		let mut load = Self::default();

		for line in lines {
			// we're not interested in the non-CPU values in /proc/stat
			if !line.starts_with("cpu") {
				break;
			}

			// split the line by spaces and convert all entries to doubles, discarding those that can't be parsed (including
			// the label at the start)
			let line: Vec<_> = line.split(' ').filter_map(|value| f64::from_str(value).ok()).collect();

			// we need to subtract 1 from IDLE_COLUMN to account for removing the CPU ID label on the previous line
			load.idle += line.get(IDLE_COLUMN - 1).context("Unknown /proc/stat format")?;
			load.total += line.iter().sum::<f64>();
		}

		Ok(load)
	}

	/// Calculates and returns CPU load by calculating idle time as a percentage of total time spent.
	pub fn load(&self) -> f64 { (self.idle / self.total).mul_add(-100.0, 100.0) }

	/// Creates a new [`MeasuredUsage`] that consists of the difference between the two given [`MeasuredUsage`]s.
	pub fn delta(old: &Self, new: &Self) -> Self {
		Self {
			total: new.total - old.total,
			idle: new.idle - old.idle,
		}
	}
}

impl Debug for MeasuredUsage {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { write!(f, "(Idle:{}, Total:{})", self.idle, self.total) }
}
