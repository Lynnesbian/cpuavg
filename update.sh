#!/bin/bash

set -e
cargo sweep -i || true
cargo update
cargo build --release --all
strip target/release/{client,server}
